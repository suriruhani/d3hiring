const sql = require("./db.js");

// constructor
const Teacher = function(teacher) {
    this.email = teacher.email;
};

Teacher.create = (newTeacher, result) => {
    sql.query("INSERT INTO teachers SET ?", newteacher, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("created Teacher: ", { id: res.insertId, ...newTeacher });
        result(null, { id: res.insertId, ...newTeacher });
    });
};

Teacher.findById = (TeacherId, result) => {
    sql.query(`SELECT * FROM teachers WHERE id = ${teacherId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found Teacher: ", res[0]);
            result(null, res[0]);
            return;
        }

        // not found Teacher with the id
        result({ kind: "not_found" }, null);
    });
};

Teacher.getAll = result => {
    sql.query("SELECT * FROM teachers", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log("Teachers: ", res);
        result(null, res);
    });
};

Teacher.updateById = (id, Teacher, result) => {
    sql.query(
        "UPDATE teachers SET email = ? WHERE id = ?",
        [teacher.email],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found Teacher with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated Teacher: ", { id: id, ...teacher });
            result(null, { id: id, ...teacher });
        }
    );
};

Teacher.remove = (id, result) => {
    sql.query("DELETE FROM teachers WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found Teacher with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted Teacher with id: ", id);
        result(null, res);
    });
};

Teacher.removeAll = result => {
    sql.query("DELETE FROM teachers", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} Teachers`);
        result(null, res);
    });
};

module.exports = Teacher;